﻿$(document).ready(function () {
    $('#strFilter').change(function () {
        var param = $(this).val();
        
        $.ajax({
            cache: false,
            type: "GET",
            url: "ListHighest/" ,
            data: {"str":param},
            dataType:  "json" ,
            success: function (data)
            {
                $('#list').html('');
                $('#list').append('<table class="table"><tr><th>Person</th><th>Hours</th></tr>');
                $(data).each(function (i, v) {
                    $('#list > table').append('<tr><td>'+v.PersonFullName+'</td><td>'+v.TotalHoursFormat+'</td></tr>');
                });
            }
        });
    });
})