﻿using EventTracker.BLL;
using EventTracker.Models;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace EventTracker.Controllers
{
    public class EventsController : Controller
    {
        private EventBLL eventBLL = new EventBLL();
        
        // GET: Events
        public ActionResult Index()
        {
            return View(eventBLL.ListAllEvents());
        }

        // GET: Events/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = eventBLL.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // GET: Events/Copy
        [Authorize()]
        public ActionResult Copy()
        {
            ViewBag.EventFromId = new SelectList(eventBLL.ListAllEvents(), "EventId", "EventName");
            ViewBag.EventToId   = new SelectList(eventBLL.ListAllEvents(), "EventId", "EventName");
            return View();
        }

        // POST: Events/Copy
        [HttpPost, ActionName("Copy")]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Copy(int EventFromId, int EventToId)
        {
            ActivityBLL actBLL = new ActivityBLL();
            actBLL.CopyActivitiesFromOtherProject(EventFromId, EventToId);
            return RedirectToAction("Index");
        }

        // GET: Events/Create
        [Authorize()]
        public ActionResult Create()
        {
            PersonBLL personBLL = new PersonBLL();
            ViewBag.EventManagerID = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text  = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString()
            });
            return View();
        }

        // POST: Events/Create
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EventId,EventName,Description,StartDate,EndDate,EventManagerID")] Event @event)
        {
            if (ModelState.IsValid)
            {
                eventBLL.Create(@event);
                return RedirectToAction("Index");
            }

            PersonBLL personBLL = new PersonBLL();
            ViewBag.EventManagerID = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text  = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (@event.EventManagerID == p.PersonId)?true:false
            });
            return View(@event);
        }

        // GET: Events/Edit/5
        [Authorize()]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Event @event = eventBLL.Find(id);

            if (@event == null)
            {
                return HttpNotFound();
            }

            PersonBLL personBLL = new PersonBLL();
            ViewBag.EventManagerID = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (@event.EventManagerID == p.PersonId) ? true : false
            });
            return View(@event);
        }

        // POST: Events/Edit/5
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EventId,EventName,Description,StartDate,EndDate,EventManagerID")] Event @event)
        {
            if (ModelState.IsValid)
            {
                eventBLL.Edit(@event);
                return RedirectToAction("Index");
            }

            PersonBLL personBLL = new PersonBLL();
            ViewBag.EventManagerID = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (@event.EventManagerID == p.PersonId) ? true : false
            });
            return View(@event);
        }

        // GET: Events/Delete/5
        [Authorize()]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Event @event = eventBLL.Find(id);

            if (@event == null)
            {
                return HttpNotFound();
            }

            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eventBLL.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
