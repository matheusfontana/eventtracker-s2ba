﻿using System.Web.Mvc;

namespace EventTracker.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult Index()
        {
            return View("DetailedError");
        }

        public ViewResult Error400()
        {
            Response.StatusCode = 400;
            return View("Error400");
        }

        public ViewResult Error403()
        {
            Response.StatusCode = 403;
            return View("Error403");
        }

        public ViewResult Error404()
        {
            Response.StatusCode = 404;
            return View("Error404");
        }

        public ViewResult Error500()
        {
            Response.StatusCode = 500;
            return View("Error500");
        }
    }
}