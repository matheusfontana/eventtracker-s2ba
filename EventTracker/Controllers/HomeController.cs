﻿using EventTracker.BLL;
using EventTracker.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EventTracker.Controllers
{
    public class HomeController : Controller
    {
        private IEventBLL eventBLL;
        private IActivityBLL actBLL;

        public HomeController(IActivityBLL aContext, IEventBLL eContext)
        {
            eventBLL = eContext;
            actBLL = aContext;
        }

        public HomeController()
        {
            eventBLL = new EventBLL();
            actBLL = new ActivityBLL();
        }


        public ActionResult Index()
        {
            var tupla = new Tuple<List<Event>, List<Activity>, List<Event>, List<Activity>>(eventBLL.ListActiveEvents(), 
                                                                                            actBLL.ListActiveActivities(),
                                                                                            eventBLL.ListUpcomingEvents(),
                                                                                            actBLL.ListUpcomingActivities());
            return View("Index", tupla);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("About");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("Contact");
        }
    }
}