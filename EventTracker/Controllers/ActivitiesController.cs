﻿using EventTracker.BLL;
using EventTracker.Models;
using System.Net;
using System.Web.Mvc;

namespace EventTracker.Controllers
{
    public class ActivitiesController : Controller
    {
        private ActivityBLL actBLL = new ActivityBLL();

        // GET: Activities
        [Authorize()]
        public ActionResult Index()
        {
            return View(actBLL.ListAllActivities());
        }

        // GET: Activities/Details/5
        [Authorize()]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = actBLL.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // GET: Activities/Create
        [Authorize()]
        public ActionResult Create()
        {
            EventBLL eventBLL = new EventBLL();
            ViewBag.EventId = new SelectList(eventBLL.ListAllEvents(), "EventId", "EventName");
            return View();
        }

        // POST: Activities/Create
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ActivityId,ActivityName,ActivityDescription,StartDate,EndDate,EventId")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                actBLL.Create(activity);
                return RedirectToAction("Index");
            }

            EventBLL eventBLL = new EventBLL();
            ViewBag.EventId = new SelectList(eventBLL.ListAllEvents(), "EventId", "EventName", activity.EventId);
            return View(activity);
        }

        // GET: Activities/Edit/5
        [Authorize()]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = actBLL.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            EventBLL eventBLL = new EventBLL();
            ViewBag.EventId = new SelectList(eventBLL.ListAllEvents(), "EventId", "EventName", activity.EventId);
            return View(activity);
        }

        // POST: Activities/Edit/5
        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ActivityId,ActivityName,ActivityDescription,StartDate,EndDate,EventId")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                actBLL.Edit(activity);
                return RedirectToAction("Index");
            }
            EventBLL eventBLL = new EventBLL();
            ViewBag.EventId = new SelectList(eventBLL.ListAllEvents(), "EventId", "EventName", activity.EventId);
            return View(activity);
        }

        // GET: Activities/Delete/5
        [Authorize()]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = actBLL.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            actBLL.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
