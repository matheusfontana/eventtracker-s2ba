﻿using EventTracker.BLL;
using EventTracker.Models;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace EventTracker.Controllers
{
    public class TeamsController : Controller
    {
        private TeamBLL teamBLL = new TeamBLL();

        // GET: Teams
        [Authorize()]
        public ActionResult Index()
        {
            return View(teamBLL.ListAllTeams());
        }

        // GET: Teams/Details/5
        [Authorize()]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = teamBLL.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // GET: Teams/Create
        [Authorize()]
        public ActionResult Create()
        {
            PersonBLL personBLL = new PersonBLL();
            ViewBag.TeamManagerId = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString()
            });
            return View();
        }

        // POST: Teams/Create

        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TeamId,TeamName,TeamManagerId")] Team team)
        {
            if (ModelState.IsValid)
            {
                teamBLL.Create(team);
                return RedirectToAction("Index");
            }

            PersonBLL personBLL = new PersonBLL();
            ViewBag.TeamManagerId = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (team.TeamManagerId == p.PersonId) ? true : false
            });
            return View(team);
        }

        // GET: Teams/Edit/5
        [Authorize()]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = teamBLL.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            PersonBLL personBLL = new PersonBLL();
            ViewBag.TeamManagerId = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (team.TeamManagerId == p.PersonId) ? true : false
            });
            return View(team);
        }

        // POST: Teams/Edit/5

        [HttpPost]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TeamId,TeamName,TeamManagerId")] Team team)
        {
            if (ModelState.IsValid)
            {
                teamBLL.Edit(team);
                return RedirectToAction("Index");
            }
            PersonBLL personBLL = new PersonBLL();
            ViewBag.EventManagerID = personBLL.ListAllPeople().Select(p => new SelectListItem
            {
                Text = p.FirstName + " " + p.LastName,
                Value = p.PersonId.ToString(),
                Selected = (team.TeamManagerId == p.PersonId) ? true : false
            });
            return View(team);
        }

        // GET: Teams/Delete/5
        [Authorize()]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = teamBLL.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize()]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            teamBLL.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
