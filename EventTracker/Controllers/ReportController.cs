﻿using EventTracker.BLL;
using EventTracker.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System;

namespace EventTracker.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private TimeSheetBLL timeSheetBLL;

        public ReportController()
        {
            timeSheetBLL = new TimeSheetBLL();
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        // GET: Project's total hours Report
        public ActionResult Highest()
        {
            ViewBag.strFilter = new SelectListItem[]{
                              new SelectListItem { Value = "Y", Text = "Year" },
                              new SelectListItem { Value = "M", Text = "Month" },
                              new SelectListItem { Value = "W", Text = "Week" },
                          };

            return View("Highest", new List<DTO.HighestWorkRateDTO>());
        }

        public JsonResult ListHighest(string str)
        {

            List<DTO.HighestWorkRateDTO> list = this.timeSheetBLL.RetrieveHighestWorkRate(str);

            foreach (var item in list)
            {
                TimeSpan ts = TimeSpan.FromMinutes(item.TotalHours);
                item.TotalHoursFormat = string.Format("{0:hh\\:mm}", ts); 
            }
            return Json(list, JsonRequestBehavior.AllowGet);

        }
    }
}