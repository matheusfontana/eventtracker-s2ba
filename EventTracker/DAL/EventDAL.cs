﻿using EventTracker.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EventTracker.DAL
{
    public class EventDAL:IEventDAL
    {
        public List<Event> ListAllEvents()
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                List<Event> lista = context.Event.OrderBy(e => e.EventId).ThenBy(e => e.StartDate).Include(e => e.Person).ToList();
                if (lista == null)
                {
                    return new List<Event>();
                }
                return lista;
            }
        }

        public void Create(Event e)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                e.CreateDate = DateTime.Now;
                context.Event.Add(e);
                context.SaveChanges();
            }
        }

        public Event Find(int? id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Event evento = new Event();
                if(id != null)
                {
                    evento = context.Event.Include(p => p.Person).SingleOrDefault(e => e.EventId == id); 
                }
                return evento;
            }
        }

        public void Delete(int id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                Event events = context.Event.Find(id);
                context.Event.Remove(events);
                context.SaveChanges();
            }
        }

        public void Edit(Event e)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                e.CreateDate = DateTime.Now;
                context.Entry(e).State = EntityState.Modified;
                context.SaveChanges();
            }
        }


        public List<Event> ListUpcomingEvents()
        {
            using (EntitiesContext context = new EntitiesContext())
            {

                List<Event> lista = context.Event.Where(e => DateTime.Compare(e.StartDate, DateTime.Now) > 0)
                                                       .Take(5)
                                                       .ToList();
                if (lista == null)
                {
                    return new List<Event>();
                }
                return lista;
            }
        }

        public List<Event> ListActiveEvents()
        {
            using (EntitiesContext context = new EntitiesContext())
            {

                List<Event> lista = context.Event.Where(e => (e.EndDate > (DateTime?)DateTime.Now)
                                                                   && DateTime.Compare(e.StartDate, DateTime.Now) < 0)
                                                       .Take(5)
                                                       .ToList();
                if (lista == null)
                {
                    return new List<Event>();
                }
                return lista;
            }
        }
    }
}