﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.DAL
{
    public interface ITeamDAL
    {
        List<Team> ListAllTeams();
        void Create(Team t);
        Team Find(int? id);
        void Delete(int id);
        void Edit(Team t);
    }
}
