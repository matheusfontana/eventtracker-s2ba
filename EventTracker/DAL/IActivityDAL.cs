﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.DAL
{
    public interface IActivityDAL
    {
        List<Activity> ListAllActivities();
        List<Activity> ListActiveActivities();
        List<Activity> ListUpcomingActivities();
        void Create(Activity a);
        Activity Find(int? id);
        void Delete(int id);
        void Edit(Activity a);
        void CopyActivitiesFromOtherProject(int from, int to);
  
    }
}
