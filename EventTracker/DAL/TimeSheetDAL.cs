﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventTracker.Models;
using System.Data.Entity;
using EventTracker.DTO;

namespace EventTracker.DAL
{
    public class TimeSheetDAL : ITimeSheetDAL
    {
        public List<TimeSheet> ListAllTimeSheets()
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                List<TimeSheet> lists = context.TimeSheet.OrderBy(p => p.Person.FirstName)
                                                         .ThenBy(a => a.Day)
                                                         .ThenByDescending(ts => ts.HoursWorked)
                                                         .Include(a => a.Activity)
                                                         .Include(r => r.Role)
                                                         .Include(p => p.Person)
                                                         .Include(t => t.Team)
                                                         .ToList();
                if (lists == null)
                {
                    return new List<TimeSheet>();
                }
                return lists;

            }
        }

        public void Create(TimeSheet ts)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.TimeSheet.Add(ts);
                context.SaveChanges();
            }
        }

        public TimeSheet Find(int? id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                TimeSheet timesheet = new TimeSheet();
                if (id != null)
                {
                    timesheet = context.TimeSheet
                                .Include(a => a.Activity)
                                .Include(r => r.Role)
                                .Include(p => p.Person)
                                .Include(t => t.Team)
                                .SingleOrDefault(ts => ts.TimeSheetId == id);
                }
                return timesheet;
            }
        }

        public void Delete(int id)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.TimeSheet.Remove(context.TimeSheet.Find(id));
                context.SaveChanges();
            }
        }

        public void Edit(TimeSheet ts)
        {
            using (EntitiesContext context = new EntitiesContext())
            {
                context.Entry(ts).State = EntityState.Modified;
                context.SaveChanges();
            }
        }


        public List<HighestWorkRateDTO> RetrieveHighestWorkRate(string param)
        {
            List<HighestWorkRateDTO> lista = new List<HighestWorkRateDTO>();
            if (param == "Y")
            {
                using (EntitiesContext context = new EntitiesContext())
                {
                    lista = (from ts in context.TimeSheet
                             where ts.Day <= DateTime.Now && ts.Day >= DbFunctions.AddDays(DateTime.Now, -365)
                             group ts by new { ts.PersonId } into gb


                             select new HighestWorkRateDTO
                             {
                                 TotalHours = gb.Sum(x => (x.HoursWorked.Hour * 60) + (x.HoursWorked.Minute)),

                                 PersonFullName = (from p in context.Person
                                                   where (p.PersonId == gb.Key.PersonId)
                                                   select p.FirstName + " " + p.LastName).FirstOrDefault()


                             }).OrderByDescending(w => w.TotalHours).Take(5).ToList();
                }
            }
            else if (param == "M")
            {
                using (EntitiesContext context = new EntitiesContext())
                {
                    lista = (from ts in context.TimeSheet
                             where ts.Day <= DateTime.Now && ts.Day >= DbFunctions.AddDays(DateTime.Now, -30)
                             group ts by new { ts.PersonId } into gb


                             select new HighestWorkRateDTO
                             {
                                 TotalHours = gb.Sum(x => (x.HoursWorked.Hour * 60) + (x.HoursWorked.Minute)),

                                 PersonFullName = (from p in context.Person
                                                   where (p.PersonId == gb.Key.PersonId)
                                                   select p.FirstName + " " + p.LastName).FirstOrDefault()


                             }).OrderByDescending(w => w.TotalHours).Take(5).ToList();
                }
            }
            else if(param == "W")
            {
                using (EntitiesContext context = new EntitiesContext())
                {
                    lista = (from ts in context.TimeSheet
                             where ts.Day <= DateTime.Now && ts.Day >= DbFunctions.AddDays(DateTime.Now, -7)
                             group ts by new { ts.PersonId } into gb


                             select new HighestWorkRateDTO
                             {
                                 TotalHours = gb.Sum(x => (x.HoursWorked.Hour * 60) + (x.HoursWorked.Minute)),

                                 PersonFullName = (from p in context.Person
                                                   where (p.PersonId == gb.Key.PersonId)
                                                   select p.FirstName + " " + p.LastName).FirstOrDefault()


                             }).OrderByDescending(w => w.TotalHours).Take(5).ToList();
                }
            }

            return lista;
        }
    }
}