﻿using EventTracker.DAL;
using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public class ActivityBLL : IActivityBLL
    {

        private IActivityDAL _actDAL;

        public ActivityBLL()
        {
            this._actDAL = new ActivityDAL();
        }

        public List<Activity> ListAllActivities()
        {
            return this._actDAL.ListAllActivities();
        }

        public List<Activity> ListActiveActivities()
        {
            return this._actDAL.ListActiveActivities();
        }

        public List<Activity> ListUpcomingActivities()
        {
            return this._actDAL.ListUpcomingActivities();
        }

        public void Create(Activity a)
        {
            this._actDAL.Create(a);
        }

        public Activity Find(int? id)
        {
            return this._actDAL.Find(id);
        }

        public void Delete(int id)
        {
            this._actDAL.Delete(id);
        }

        public void Edit(Activity a)
        {
            this._actDAL.Edit(a);
        }

        public void CopyActivitiesFromOtherProject(int from, int to)
        {
            this._actDAL.CopyActivitiesFromOtherProject(from, to);
        }
    }
}