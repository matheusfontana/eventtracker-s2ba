﻿using EventTracker.DAL;
using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public class PersonBLL : IPersonBLL
    {
        private IPersonDAL _personDAL;

        public PersonBLL()
        {
            this._personDAL = new PersonDAL();
        }

        public List<Person> ListAllPeople()
        {
            return this._personDAL.ListAllPeople();
        }


        public void Create(Person p)
        {
            this._personDAL.Create(p);
        }


        public Person Find(int? id)
        {
            return this._personDAL.Find(id);
        }

        public void Delete(int id)
        {
            this._personDAL.Delete(id);
        }

        public void Edit(Person p)
        {
            this._personDAL.Edit(p);
        }


    }
}