﻿using EventTracker.DAL;
using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public class EventBLL : IEventBLL
    {
        private IEventDAL _eventDAL;

        public EventBLL()
        {
            this._eventDAL = new EventDAL();
        }

        public List<Event> ListAllEvents()
        {
            return this._eventDAL.ListAllEvents();
        }

        public void Create(Event e)
        {
            this._eventDAL.Create(e);
        }

        public Event Find(int? id)
        {
            return this._eventDAL.Find(id);
        }

        public void Delete(int id)
        {
            this._eventDAL.Delete(id);
        }

        public void Edit(Event e)
        {
            this._eventDAL.Edit(e);
        }


        public List<Event> ListUpcomingEvents()
        {
            return this._eventDAL.ListUpcomingEvents();
        }

        public List<Event> ListActiveEvents()
        {
            return this._eventDAL.ListActiveEvents();
        }
    }
}