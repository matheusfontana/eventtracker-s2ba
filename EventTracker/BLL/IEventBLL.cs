﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public interface IEventBLL
    {
        List<Event> ListAllEvents();
        List<Event> ListUpcomingEvents();
        List<Event> ListActiveEvents();
        void Create(Event e);
        Event Find(int? id);
        void Delete(int id);
        void Edit(Event e);
    }
}
