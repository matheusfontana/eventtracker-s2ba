﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public interface ITeamBLL
    {
        List<Team> ListAllTeams();
        void Create(Team t);
        Team Find(int? id);
        void Delete(int id);
        void Edit(Team t);
    }
}
