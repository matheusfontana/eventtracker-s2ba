﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public interface IRoleBLL
    {
        List<Role> ListAllRoles();
        void Create(Role r);
        Role Find(int? id);
        void Delete(int id);
        void Edit(Role r);
    }
}
