﻿using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public interface ITimeSheetBLL
    {
        List<TimeSheet> ListAllTimeSheets();
        void Create(TimeSheet ts);
        TimeSheet Find(int? id);
        void Delete(int id);
        void Edit(TimeSheet ts);
        List<DTO.HighestWorkRateDTO> RetrieveHighestWorkRate(string param);
    }
}
