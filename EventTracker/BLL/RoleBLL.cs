﻿using EventTracker.DAL;
using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public class RoleBLL : IRoleBLL
    {
        private IRoleDAL _roleDAL;

        public RoleBLL()
        {
            this._roleDAL = new RoleDAL();
        }

        public List<Role> ListAllRoles()
        {
            return this._roleDAL.ListAllRoles();
        }

        public void Create(Role r)
        {
            this._roleDAL.Create(r);
        }

        public Role Find(int? id)
        {
            return this._roleDAL.Find(id);
        }

        public void Delete(int id)
        {
            this._roleDAL.Delete(id);
        }

        public void Edit(Role r)
        {
            this._roleDAL.Edit(r);
        }
    }
}