﻿using EventTracker.DAL;
using EventTracker.Models;
using System.Collections.Generic;

namespace EventTracker.BLL
{
    public class TeamBLL : ITeamBLL
    {
        private ITeamDAL _teamDAL;

        public TeamBLL()
        {
            this._teamDAL = new TeamDAL();
        }

        public List<Team> ListAllTeams()
        {
            return this._teamDAL.ListAllTeams();
        }

        public void Create(Team t)
        {
            this._teamDAL.Create(t);
        }

        public Team Find(int? id)
        {
            return this._teamDAL.Find(id);
        }

        public void Delete(int id)
        {
            this._teamDAL.Delete(id);
        }

        public void Edit(Team t)
        {
            this._teamDAL.Edit(t);
        }
    }
}