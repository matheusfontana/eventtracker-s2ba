﻿using EventTracker.BLL;
using EventTracker.Controllers;
using EventTracker.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EventTracker.Tests
{
    [TestClass]
    public class PeopleControllerTests
    {
        private List<Person> people = null;

        [TestInitialize]
        public void Setup()
        {
            //This section of code models an object to be used as sample context data retriveal.
            this.people = new List<Person>();
            people.Add(new Person { PersonId = 0, FirstName = "teste0", LastName = "0" });
            people.Add(new Person { PersonId = 1, FirstName = "teste1", LastName = "1" });
            people.Add(new Person { PersonId = 2, FirstName = "teste2", LastName = "2" });
        }

        [TestMethod]
        public void Action_Index_Return_Correct_View()
        {
            //This test checks that the action returns the correct view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();
            personRepository.Setup(x => x.ListAllPeople()).Returns(this.people);

            PeopleController controller = new PeopleController(personRepository.Object);

            //Act
            var result = controller.Index() as ViewResult;

            //Assert
            Assert.AreEqual("Index", result.ViewName);
        }

        [TestMethod]
        public void Action_Index_Returns_Correct_Model_Type()
        {
            //This test checks that the action Index passes a list of people to the view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();

            PeopleController controller = new PeopleController(personRepository.Object);
            personRepository.Setup(x => x.ListAllPeople()).Returns(this.people);

            //Act
            var result = controller.Index() as ViewResult;

            //Assert
            Assert.AreEqual(typeof(List<Person>), result.Model.GetType());
        }

        [TestMethod]
        public void Action_Details_Return_Correct_View()
        {
            //This test checks that the action returns the correct view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();
            personRepository.Setup(x => x.Find(1)).Returns(this.people.Find(delegate(Person p) { return p.PersonId == 1; }));

            PeopleController controller = new PeopleController(personRepository.Object);

            //Act
            var result = controller.Details(1) as ViewResult;

            //Assert
            Assert.AreEqual("Details", result.ViewName);
        }

        [TestMethod]
        public void Action_Details_Returns_Correct_Model_Type()
        {
            //This test checks that the action Details passes a Person object to the view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();

            PeopleController controller = new PeopleController(personRepository.Object);
            personRepository.Setup(x => x.Find(1)).Returns(this.people.Find(delegate(Person p) { return p.PersonId == 1; }));

            //Act
            var result = controller.Details(1) as ViewResult;

            //Assert
            Assert.AreEqual(typeof(Person), result.Model.GetType());
        }

        [TestMethod]
        public void Action_Create_Return_Correct_View()
        {
            //This test checks that the action returns the correct view.
            //Arrange
            PeopleController controller = new PeopleController();

            //Act
            var result = controller.Create() as ViewResult;

            //Assert
            Assert.AreEqual("Create", result.ViewName);
        }

        [TestMethod]
        public void Action_Edit_Return_Correct_View()
        {
            //This test checks that the action returns the correct view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();
            personRepository.Setup(x => x.Find(1)).Returns(this.people.Find(delegate(Person p) { return p.PersonId == 1; }));

            PeopleController controller = new PeopleController(personRepository.Object);

            //Act
            var result = controller.Edit(1) as ViewResult;

            //Assert
            Assert.AreEqual("Edit", result.ViewName);
        }

        [TestMethod]
        public void Action_Edit_Returns_Correct_Model_Type()
        {
            //This test checks that the action Edit passes a Person object to the view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();

            PeopleController controller = new PeopleController(personRepository.Object);
            personRepository.Setup(x => x.Find(1)).Returns(this.people.Find(delegate(Person p) { return p.PersonId == 1; }));

            //Act
            var result = controller.Edit(1) as ViewResult;

            //Assert
            Assert.AreEqual(typeof(Person), result.Model.GetType());
        }

        [TestMethod]
        public void Action_Delete_Return_Correct_View()
        {
            //This test checks that the action returns the correct view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();
            personRepository.Setup(x => x.Find(1)).Returns(this.people.Find(delegate(Person p) { return p.PersonId == 1; }));

            PeopleController controller = new PeopleController(personRepository.Object);

            //Act
            var result = controller.Delete(1) as ViewResult;

            //Assert
            Assert.AreEqual("Delete", result.ViewName);
       }

        [TestMethod]
        public void Action_Delete_Returns_Correct_Model_Type()
        {
            //This test checks that the action Delete passes a Person object to the view.
            //Arrange
            Mock<IPersonBLL> personRepository = new Mock<IPersonBLL>();

            PeopleController controller = new PeopleController(personRepository.Object);
            personRepository.Setup(x => x.Find(1)).Returns(this.people.Find(delegate(Person p) { return p.PersonId == 1; }));

            //Act
            var result = controller.Delete(1) as ViewResult;

            //Assert
            Assert.AreEqual(typeof(Person), result.Model.GetType());
        }
    }
}
